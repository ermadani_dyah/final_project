<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('content.index');
// });
// Route::get('/all', function () {
//     return view('content.all_berita');
// });
// Route::get('/hair', function () {
//     return view('content.hair');
// });
// Route::get('/inspiration', function () {
//     return view('content.inspiration');
// });
// Route::get('/look_for_less', function () {
//     return view('content.look_for_less');
// });
// Route::get('/make_up', function () {
//     return view('content.make_up');
// });
// Route::get('/skin', function () {
//     return view('content.skin');
// });
// Route::get('/style_trends', function () {
//     return view('content.style_trends');
// });
// Route::get('/healthy', function () {
//     return view('content.healthy');
// });
// Route::get('/working_life', function () {
//     return view('content.working_life');
// });
// Route::get('/admin', function () {
//     return view('content.admin.Halaman_Admin');
// });
// Route::get('/admin/all', function () {
//     return view('content.admin.all_berita.all');
// });
// Route::get('/admin/new', function () {
//     return view('content.admin.new_berita.new');
// });
// Route::get('/admin/trending', function () {
//     return view('content.admin.trending_berita.trending');
// });
// Route::get('/admin/top_week', function () {
//     return view('content.admin.top_week_berita.top_week');
// });

//Router  Halaman All Berita
Route::get('/all', function () {
    return view('admin.all');
})->middleware('auth')->name('all');

//Router  Halaman New Berita
Route::get('/new', function () {
    return view('admin.new');
})->middleware('auth')->name('new');

//Router  Halaman Trending Berita
Route::get('/trending', function () {
    return view('admin.trending');
})->middleware('auth')->name('trending');

//Router  Halaman Top Week Berita
Route::get('/top_week', function () {
    return view('admin.top_week');
})->middleware('auth')->name('top_week');

//halaman Dashboard
Route::get('/admin','HalamanAdminController@count_all')->name('halaman_admin');

//Router Halaman Home
Route::get('/', function () {
    return view('content.welcome');
});

//Router Halaman Hair
Route::get('/hair', function () {
    return view('content.hair');
});

//Router Halaman Inspiration
Route::get('/inspiration', function () {
    return view('content.inspiration');
});

//Router Halaman look_for_less
Route::get('/look_for_less', function () {
    return view('content.look_for_less');
});

//Router Halaman make_up
Route::get('/make_up', function () {
    return view('content.make_up');
});

//Router Halaman skin
Route::get('/skin', function () {
    return view('content.skin');
});

//Router Halaman Style Trends
Route::get('/style_trends', function () {
    return view('content.style_trends');
});

//Router Halaman Healthy
Route::get('/healthy', function () {
    return view('content.healthy');
});

//Router Halaman Working Life
Route::get('/working_life', function () {
    return view('content.working_life');
});

//Router Halaman Working Life
Route::get('/all_berita', function () {
    return view('content.all_berita');
});

Route::get('/register','AuthController@getRegister')->name('register')->middleware('guest');
Route::post('/register','AuthController@postRegister')->middleware('guest');
Route::get('/login','AuthController@getLogin')->name('login')->middleware('guest');
Route::post('/login','AuthController@postLogin')->middleware('guest');
Route::get('/logout','AuthController@logout')->name('logout')->middleware('auth');
// Route::get('/', function () {
//     return view('login.login');
// });
//Router Halaman Home
// Route::get('/','HomeController@index');
// Route::get('/all','HomeController@all');
// Route::get('/hair','HomeController@hair');
// Route::get('/inspiration','HomeController@inspiration');
// Route::get('/look_for_less','HomeController@look_for_less');
// Route::get('/make_up','HomeController@make_up');
// Route::get('/skin','HomeController@skin');
// Route::get('/style_trends','HomeController@style_trends');
// Route::get('/healthy','HomeController@healthy');
// Route::get('/woorking_life','HomeController@working_life');

//Router Halaman Admin New Berita
// Route::get('/admin/new/tambah','NewController@tambah');
// Route::post('/admin/new','NewController@store');
// Route::get('/admin/new','NewController@index');
// Route::get('/admin/new/{all_id}','NewController@detail');
// Route::get('/admin/new/{id}/edit','NewController@edit');
// Route::put('/admin/new/{id}','NewController@update');
// Route::delete('/admin/new/{id}','NewController@delate');

// //Router Halaman Admin New Berita
// Route::get('/admin/trending/tambah','TrendingController@tambah');
// Route::post('/admin/trending','TrendingController@store');
// Route::get('/admin/trending','TrendingController@index');
// Route::get('/admin/trending/{all_id}','TrendingController@detail');
// Route::get('/admin/trending/{id}/edit','TrendingController@edit');
// Route::put('/admin/trending/{id}','TrendingController@update');
// Route::delete('/admin/trending/{id}','TrendingController@delate');

// //Router Halaman Admin Toop Week Berita
// Route::get('/admin/top_week/tambah','TopWeekController@tambah');
// Route::post('/admin/top_week','TopWeekController@store');
// Route::get('/admin/top_week','TopWeekController@index');
// Route::get('/admin/top_week/{all_id}','TopWeekController@detail');
// Route::get('/admin/top_week/{id}/edit','TopWeekController@edit');
// Route::put('/admin/top_week/{id}','TopWeekController@update');
// Route::delete('/admin/top_week/{id}','TopWeekController@delate');

// Route::get('/admin','HalamanAdminController@count_all');