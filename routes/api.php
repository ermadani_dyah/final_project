<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Register
Route::post('register','Api\RegisterController@action');

//login
Route::post('login','Api\LoginController@action');
//Router Halaman Admin All Berita
Route::post('/admin/all/store','AllController@store');
Route::get('/admin/all','AllController@index');
Route::get('/admin/all/{id_all?}','AllController@show');
Route::post('/admin/all/update/{id_all?}','AllController@update');
Route::delete('/admin/all/{id_all?}','AllController@destroy');

//Router Halaman Admin New Berita
Route::post('/admin/new/store','NewController@store');
Route::get('/admin/new','NewController@index');
Route::get('/admin/new/{id_new?}','NewController@show');
Route::post('/admin/new/update/{id_new?}','NewController@update');
Route::delete('/admin/new/{id_new?}','NewController@destroy');

//Router Halaman Admin Trending Berita
Route::post('/admin/trending/store','TrendingController@store');
Route::get('/admin/trending','TrendingController@index');
Route::get('/admin/trending/{id_trending?}','TrendingController@show');
Route::post('/admin/trending/update/{id_trending?}','TrendingController@update');
Route::delete('/admin/trending/{id_trending?}','TrendingController@destroy');

//Router Halaman Admin Top Week Berita
Route::post('/admin/top_week/store','TopWeekController@store');
Route::get('/admin/top_week','TopWeekController@index');
Route::get('/admin/top_week/{id_top_week?}','TopWeekController@show');
Route::post('/admin/top_week/update/{id_top_week?}','TopWeekController@update');
Route::delete('/admin/top_week/{id_top_week?}','TopWeekController@destroy');

//Router Halaman User Home Page
Route::get('/content/home_page1','HomeController@Data1');
Route::get('/content/home_page2','HomeController@Data2');
Route::get('/content/home_page3','HomeController@Data3');
Route::get('/content/hair','HomeController@Hair');
Route::get('/content/inspiration','HomeController@inspiration');
Route::get('/content/look_for_less','HomeController@look_for_less');
Route::get('/content/make_up','HomeController@make_up');
Route::get('/content/skin','HomeController@skin');
Route::get('/content/style_trends','HomeController@style_trends');
Route::get('/content/healthy','HomeController@healthy');
Route::get('/content/working_life','HomeController@working_life');
Route::get('/content/all_menu','HomeController@All_berita');