<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllBeritasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_beritas', function (Blueprint $table) {
            $table->bigIncrements('id_all');
            $table->string('title');
            $table->string('content');
            $table->string('gambar');
            $table->string('link');
            $table->string('tanggal');
            $table->string('kategori');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_beritas');
    }
}
