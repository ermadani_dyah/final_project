<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\All;
use Illuminate\Support\Facades\Validator;
class AllController extends Controller
{
    public function index(){
        $all=All::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$all
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required|',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required',
                'tanggal'=>'required',
                'kategori'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! ",
                'tanggal.required'=>"Masukan Tanggal Berita! ",
                'kategori.required'=>"Masukan Kategori Berita! ",
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $all=All::create([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar'),
                "tanggal"=>$request->input('tanggal'),
                "kategori"=>$request->input('kategori')
            ]);
            if($all){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $all=All::whereid_all($id)->first();
        if($all){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$all
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_all){
        $all=All::findOrFail($id_all);
        $all->delete();
        if($all){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required',
                'tanggal'=>'required',
                'kategori'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! ",
                'tanggal.required'=>"Masukan Tanggal Berita! ",
                'kategori.required'=>"Masukan Kategori Berita! ",
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $all=All::whereid_all($request->input('id_all'))->update([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar'),
                "tanggal"=>$request->input('tanggal'),
                "kategori"=>$request->input('kategori')
            ]);
            if($all){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    }
    
}
