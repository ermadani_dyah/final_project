<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\All;
use App\Trending;
use App\Top_week;
use App\Baru;
class HomeController extends Controller
{
    public function Data1(){
        $new=Baru::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Berita Terbaru',
            'data'=>$new
        ],200);
    }
    public function Data2(){
        $trending=Trending::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Berita Terbaru',
            'data'=>$trending
        ],200);
    }
    public function Data3(){
        $top_week=Top_week::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Berita Terbaru',
            'data'=>$top_week
        ],200);
    }
    public function Hair(){
        $hair=All::wherekategori('Hair')->get();
        if($hair){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$hair
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function inspiration(){
        $inspiration=All::wherekategori('INSPIRATION')->get();
        if($inspiration){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$inspiration
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function look_for_less(){
        $look_for_less=All::wherekategori('Look _For_Less')->get();
        if($look_for_less){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$look_for_less
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function make_up(){
        $make_up=All::wherekategori('Make Up')->get();
        if($make_up){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$make_up
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function skin(){
        $skin=All::wherekategori('Skin')->get();
        if($skin){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$skin
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function style_trends(){
        $style_trends=All::wherekategori('Style Trends')->get();
        if($style_trends){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$style_trends
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function healthy(){
        $healthy=All::wherekategori('Healthy')->get();
        if($healthy){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$healthy
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function working_life(){
        $working_life=All::wherekategori('Working Life')->get();
        if($working_life){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$working_life
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }

    public function All_berita(){
        $all=all::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Berita Terbaru',
            'data'=>$all
        ],200);
    }

    // public function index(){
    //     $new=DB::table('new_beritas')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     $top_week=DB::table('top_week')->get();
    //     return view('content.index',compact('new','trending','top_week'));
    // }
    // public function all(){
    //     $all=DB::table('all_beritas')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.all_berita',compact('all','trending'));
    // }
    // public function hair(){
    //     $hair=DB::table('all_beritas')->where('kategori','Hair')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.hair',compact('hair','trending'));
    // }
    // public function inspiration(){
    //     $inspiration=DB::table('all_beritas')->where('kategori','INSPIRATION')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.inspiration',compact('inspiration','trending'));
    // }
    // public function look_for_less(){
    //     $look_for_less=DB::table('all_beritas')->where('kategori','Look _For_Less')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.look_for_less',compact('look_for_less','trending'));
    // }
    // public function make_up(){
    //     $make_up=DB::table('all_beritas')->where('kategori','Make Up')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.make_up',compact('make_up','trending'));
    // }
    // public function skin(){
    //     $skin=DB::table('all_beritas')->where('kategori','Skin')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.skin',compact('skin','trending'));
    // }
    // public function style_trends(){
    //     $style_trends=DB::table('all_beritas')->where('kategori','Style Trends')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.style_trends',compact('style_trends','trending'));
    // }
    // public function healthy(){
    //     $healthy=DB::table('all_beritas')->where('kategori','Healthy')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.healthy',compact('healthy','trending'));
    // }
    // public function working_life(){
    //     $working_life=DB::table('all_beritas')->where('kategori','Working Life')->get();
    //     $trending=DB::table('trending_beritas')->get();
    //     return view('content.working_life',compact('working_life','trending'));
    // }
}
