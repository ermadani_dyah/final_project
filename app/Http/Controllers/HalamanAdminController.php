<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HalamanAdminController extends Controller
{
    public function count_all(){
        $count_all=DB::table('all_beritas')->count();
        $trending=DB::table('trending_beritas')->count();
        $new=DB::table('new_beritas')->count();
        $top_week=DB::table('top_week')->count();
        return view('admin.Halaman_Admin',compact('count_all','trending','new','top_week'));
    }
}
