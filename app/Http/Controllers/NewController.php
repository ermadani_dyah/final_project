<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Baru;
use Illuminate\Support\Facades\Validator;
class NewController extends Controller
{
    public function index(){
        $new=Baru::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$new
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! "
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $new=Baru::create([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar')
            ]);
            if($new){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $new=Baru::whereid_new($id)->first();
        if($new){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$new
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_new){
        $new=Baru::findOrFail($id_new);
        $new->delete();
        if($new){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! "
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $new=Baru::whereid_new($request->input('id_new'))->update([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar')
            ]);
            if($new){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    } 
}
