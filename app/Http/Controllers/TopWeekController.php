<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Top_week;
use Illuminate\Support\Facades\Validator;
class TopWeekController extends Controller
{
    public function index(){
        $top_week=Top_week::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$top_week
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! "
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $top_week=Top_week::create([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar')
            ]);
            if($top_week){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $top_week=Top_week::whereid_top_week($id)->first();
        if($top_week){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$top_week
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_top_week){
        $top_week=Top_Week::findOrFail($id_top_week);
        $top_week->delete();
        if($top_week){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'content'=>'required',
                'gambar'=>'required',
                'link'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'content.required'=>"Masukan Isi Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! "
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $top_week=Top_week::whereid_top_week($request->input('id_top_week'))->update([
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar')
            ]);
            if($top_week){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    } 
}
