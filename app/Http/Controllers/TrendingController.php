<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trending;
use Illuminate\Support\Facades\Validator;
class TrendingController extends Controller
{
    public function index(){
        $trending=Trending::latest()->get();
        return response([
            'success'=>true,
            'message'=>'Daftar Semua Data Berita',
            'data'=>$trending
        ],200);
    }
    public function store(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'gambar'=>'required',
                'link'=>'required',
                'tanggal'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! ",
                'tanggal.required'=>"Masukan Tanggal Berita! "
            ]
        );
        if($validasi->fails()){
            return response()->json([
                'success'=>false,
                'message'=>'Silahkan Isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $trending=Trending::create([
                "title"=>$request->input('title'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar'),
                "tanggal"=>$request->input('tanggal')
            ]);
            if($trending){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Disimpan!',
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Disimpan',
                ], 400);
            }
        }
    }
    public function show($id){
        $trending=Trending::whereid_trending($id)->first();
        if($trending){
            return response()->json([
                'success'=>true,
                'message'=>'Detail Data Berita',
                'data'=>$trending
            ],200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Detail Data Berita Tidak Ditemukan!',
                'data'    => ''
            ], 404);
        }
    }
    public function destroy($id_trending){
        $trending=Trending::findOrFail($id_trending);
        $trending->delete();
        if($trending){
            return response()->json([
                'success' => true,
                'message' => 'Data Berita Berhasil Dihapus!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Berhasil Gagal Dihapus!',
            ], 500);
        }
    }
    public function update(Request $request){
        $validasi=Validator::make($request->all(),
            [
                'title'=>'required',
                'gambar'=>'required',
                'link'=>'required',
                'tanggal'=>'required'
            ],
            [
                'title.required'=>"Masukan Judul Berita! ",
                'gambar.required'=>"Masukan gambar Berita! ",
                'link.required'=>"Masukan Link Berita! ",
                'tanggal.required'=>"Masukan Tanggal Berita! "
            ]
        );
        if($validasi->fails()){
            return response()-json([
                'success'=>false,
                'message'=>'Silahkan isi Kolom Yang Kosong',
                'data'=>$validasi->errors()
            ],400);
        }else{
            $trending=Trending::whereid_trending($request->input('id_trending'))->update([
                "title"=>$request->input('title'),
                "link"=>$request->input('link'),
                "gambar"=>$request->input('gambar'),
                "tanggal"=>$request->input('tanggal')
            ]);
            if($trending){
                return response()->json([
                    'success' => true,
                    'message' => 'Data Berita Berhasil Diupdate!',
                ], 200);
            }else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Berita Gagal Diupdate!',
                ], 500);
            }
        }
    }
}
