<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class All extends Model
{
    protected $primaryKey = 'id_all';
    protected $table="all_beritas";
    protected $fillable=["title","content","gambar","link","tanggal","kategori"];
}
