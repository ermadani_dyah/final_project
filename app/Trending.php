<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trending extends Model
{
    protected $primaryKey = 'id_trending';
    protected $table="trending_beritas";
    protected $fillable=["title","tanggal","gambar","link"];
}
