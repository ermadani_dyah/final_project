<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baru extends Model
{
    protected $primaryKey = 'id_new';
    protected $table="new_beritas";
    protected $fillable=["title","content","gambar","link"];
}
