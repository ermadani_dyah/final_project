<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Top_week extends Model
{
    protected $primaryKey = 'id_top_week';
    protected $table="top_week";
    protected $fillable=["title","content","gambar","link"];
}
