require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

//All Berita
import IndexComponent from './components/all_berita/index.vue';
import CreateComponent from './components/all_berita/tambah.vue';
import EditComponent from './components/all_berita/edit.vue';

//New Berita
import IndexNewComponent from './components/new_berita/index.vue';
import CreateNewComponent from './components/new_berita/tambah.vue';
import EditNewComponent from './components/new_berita/edit.vue';

//Trending Berita
import IndexTrendingComponent from './components/trending_berita/index.vue';
import CreateTrendingComponent from './components/trending_berita/tambah.vue';
import EditTrendingComponent from './components/trending_berita/edit.vue';

//Top Week Berita
import IndexTopWeekComponent from './components/top_week_berita/index.vue';
import CreateTopWeekComponent from './components/top_week_berita/tambah.vue';
import EditTopWeekComponent from './components/top_week_berita/edit.vue';

//Home Page
import IndexHomeComponent from './components/home/index.vue';
import IndexHairComponent from './components/home/Hair.vue';
import IndexHealthyComponent from './components/home/Healthy.vue';
import IndexInspirationComponent from './components/home/Inspiration.vue';
import IndexLookForLessComponent from './components/home/Look_For_Less.vue';
import IndexMakeUpComponent from './components/home/MakeUp.vue';
import IndexSkinComponent from './components/home/Skin.vue';
import IndexStyleTrendsComponent from './components/home/Style_Trends.vue';
import IndexWorkingLifeComponent from './components/home/WorkingLife.vue';
import IndexAllBeritaComponent from './components/home/AllBerita.vue';
const routes = [
    //------->All Berita<-------//
    {
        name: 'all',
        path: '/all',
        component: IndexComponent
    },
    {
        name: 'tambahAllBerita',
        path: '/TambahAll',
        component: CreateComponent
    },
    {
        name: 'editAllBerita',
        path: '/EditAll/:id_all',
        component: EditComponent
    },
    //------->New Berita<-------//
    {
        name: 'new',
        path: '/new',
        component: IndexNewComponent
    },
    {
        name: 'tambahNewBerita',
        path: '/TambahNew',
        component: CreateNewComponent
    },
    {
        name: 'editNewBerita',
        path: '/EditNew/:id_new',
        component: EditNewComponent
    },
    //------->Trending Berita<-------//
    {
        name: 'trending',
        path: '/trending',
        component: IndexTrendingComponent
    },
    {
        name: 'tambahTrendingBerita',
        path: '/TambahTrending',
        component: CreateTrendingComponent
    },
    {
        name: 'editTrendingBerita',
        path: '/EditTrending/:id_trending',
        component: EditTrendingComponent
    },
    //------->Top Week Berita<-------//
    {
        name: 'top_week',
        path: '/top_week',
        component: IndexTopWeekComponent
    },
    {
        name: 'tambahTopWeekBerita',
        path: '/TambahTopWeek',
        component: CreateTopWeekComponent
    },
    {
        name: 'editTopWeekBerita',
        path: '/EditTopWeek/:id_top_week',
        component: EditTopWeekComponent
    },
    //------->Home<-------//
    {
        name: 'home',
        path: '/',
        component: IndexHomeComponent
    },
    {
        name: 'healthy',
        path: '/healthy',
        component: IndexHealthyComponent
    },
    {
        name: 'hair',
        path: '/hair',
        component: IndexHairComponent
    },
    {
        name: 'inspiration',
        path: '/inspiration',
        component: IndexInspirationComponent
    },
    {
        name: 'look_for_less',
        path: '/look_for_less',
        component: IndexLookForLessComponent
    },
    {
        name: 'make_up',
        path: '/make_up',
        component: IndexMakeUpComponent
    },
    {
        name: 'skin',
        path: '/skin',
        component: IndexSkinComponent
    },
    {
        name: 'style_trends',
        path: '/style_trends',
        component: IndexStyleTrendsComponent
    },
    {
        name: 'working_life',
        path: '/working_life',
        component: IndexWorkingLifeComponent
    },
    {
        name: 'all_berita',
        path: '/all_berita',
        component: IndexAllBeritaComponent
    },
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const all = new Vue(Vue.util.extend({ router }, App)).$mount('#all');
const baru = new Vue(Vue.util.extend({ router }, App)).$mount('#new');
const trending = new Vue(Vue.util.extend({ router }, App)).$mount('#trending');
const top_week = new Vue(Vue.util.extend({ router }, App)).$mount('#top_week');
const home = new Vue(Vue.util.extend({ router }, App)).$mount('#home1');
const hair = new Vue(Vue.util.extend({ router }, App)).$mount('#hair');
const healthy = new Vue(Vue.util.extend({ router }, App)).$mount('#healthy');
const inpiration = new Vue(Vue.util.extend({ router }, App)).$mount('#inspiration');
const look_for_less = new Vue(Vue.util.extend({ router }, App)).$mount('#look_for_less');
const make_up = new Vue(Vue.util.extend({ router }, App)).$mount('#make_up');
const skin = new Vue(Vue.util.extend({ router }, App)).$mount('#skin');
const style_trends = new Vue(Vue.util.extend({ router }, App)).$mount('#style_trends');
const working_life = new Vue(Vue.util.extend({ router }, App)).$mount('#working_life');
const all_berita = new Vue(Vue.util.extend({ router }, App)).$mount('#all_berita');