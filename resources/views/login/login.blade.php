<!doctype html>
<html lang="en">
  <!--Head-->
  @include('login.include.head')
  <!--/Head-->
  <body>
    <div class="content" id="app">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="{{asset('/images/undraw_remotely_2j6y.svg')}}" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-6 contents">
            <div class="row justify-content-center">
              <div class="col-md-8">
                <div class="mb-4">
                  <h3>Sign In</h3>
                  <p class="mb-4">Silahkan Login Menggunakan Akun Anda</p>
                </div>
                <form  method="post" action="{{route ('login')}}">
                    {{csrf_field()}}
                    <div class="form-group first">
                      <label for="username">email</label>
                      <input type="text" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group last mb-4">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="d-flex mb-5 align-items-center">
                      <span class="ml-auto">
                        <a href="{{ url('/register') }}" class="forgot-pass">Lakukanlah Registrasi Jika Tidak Mempunyai Akun</a>
                      </span> 
                    </div>
                    <input type="submit" value="Log In" class="btn btn-block btn-primary">
                    <span class="d-block text-left my-4 text-muted">&mdash; or login with &mdash;</span>
                    <div class="social-login">
                      <a href="#" class="facebook">
                        <span class="icon-facebook mr-3"></span> 
                      </a>
                      <a href="#" class="twitter">
                        <span class="icon-twitter mr-3"></span> 
                      </a>
                      <a href="#" class="google">
                        <span class="icon-google mr-3"></span> 
                      </a>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Head-->
    @include('login.include.script')
    <!--/Head-->
  </body>
</html>