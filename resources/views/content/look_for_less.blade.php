<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <!--Head-->
  @include('content.include.head')
  <!--/Head-->
  <body>
    <!--NAVBAR-->
    @include('content.include.navbar')
    <!--/NAVBAR-->
    
    <div id="look_for_less">
      
    </div>
    
    <!---FOOTER--->
    @include('content.include.footer')
    <!--/Footer-->
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
  </body>
</html>
