<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>GIRLS ONLY</title>
    <link href="{{asset('/css/styles.css')}}" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Economica' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue.min.js" integrity="sha512-Z0dNfC81uEXC2iTTXtE0rM18I3ATkwn1m8Lxe0onw/uPEEkCmVZd+H8GTeYGkAZv50yvoSR5N3hoy/Do2hNSkw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue-icons.common.js" integrity="sha512-1JotZA+Yp2KTsv3oZUMaoStKXHJbfOoOCFIVw6NjNIhbLXRYgPfHlnfCZELZSa7Pl2gERROUiVjn6goWxnh5vA==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue-icons.common.min.js" integrity="sha512-+e+Co6xInJyWHIqLkhtaBPxFk+RU1bt/ajG/x5l7UuPiPH9n9gwxmfAtTSkX30JM2dZMoM1KXKjNucsrQrS7cQ==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue-icons.css" integrity="sha512-ofvNcyNADx27Xw9edhhecFoL1M+QamynjUyIDVrkLYemVgvWpT5jMA4box8FFaH+umvQyGEDAHyv1CGpmP8NGw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-vue/2.21.2/bootstrap-vue.min.css" integrity="sha512-YnP4Ql71idaMB+/ZG38+1adSSQotdqvixQ+dQg8x/IFA4heIj6i0BC31W5T5QUdK1Uuwa01YdqdcT42q+RldAg==" crossorigin="anonymous" />
    <!----menu--->
    <link rel="stylesheet" href="{{asset('/css/superfish.css')}}" media="screen">
    <script src="{{asset('/js/jquery-1.9.0.min.js')}}"></script>
    <script src="{{asset('/js/hoverIntent.js')}}"></script>
    <script src="{{asset('/js/superfish.js')}}"></script>
    <script>
        // initialise plugins
        jQuery(function(){
          jQuery('#example').superfish({
            //useClick: true
          });
        });
    </script>
</head>