<div class="wrap5">
    <div class="container2">
        <div class="header">
            <div class="logo">
                <h1>GIRLS ONLY</h1>
            </div>
            <div class="menu">
                <ul class="sf-menu" id="example" >
                    <li>
                        <a href="/" >Home</a>
                    </li>
                    <li class="current"> 
                        <a href="#">FASHION</a>
                    <ul>
                        <li> 
                            <a href="/style_trends" >Style & Trends</a> 
                        </li>
                        <li> 
                            <a href="/look_for_less">Look For Less</a> 
                        </li>
                    </ul>
                    </li>
                    <li class="current"> 
                        <a href="#">BEAUTY</a>
                        <ul>
                            <li> 
                                <a href="/hair">Hair</a> 
                            </li>
                            <li> 
                                <a href="/skin">Skin</a> 
                            </li>
                            <li> 
                                <a href="/make_up">Make Up</a> 
                            </li>
                            <li> 
                                <a href="/healthy">Healty</a> 
                            </li>
                        </ul>
                    </li>
                    <li class="current"> 
                        <a href="#">CAREER</a>
                        <ul>
                            <li> 
                                <a href="/working_life">WORKING LIFE</a> 
                            </li>
                            <li> 
                                <a href="/inspiration">INSPIRATION</a> 
                            </li>
                        </ul>
                    </li>
                    <li> 
                        <a href="/all_berita">All NEWS</a> 
                    </li>
                    <li> 
                        <a href="/login">Admin</a> 
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearing"></div>
    </div>
    <div class="clearing"></div>
</div>