<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin/halaman_utama" class="brand-link">
        <img src="{{asset('../dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Admin Berita Girls</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
            <a href="{{ url('/') }}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p> Dashboard</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="{{ url('/all') }}" class="nav-link">
                <i class="nav-icon far fa-newspaper"></i>
                <p>All Berita</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="{{ url('/top_week') }}" class="nav-link">
                <i class="nav-icon fas fa-trophy"></i>
                <p>Top Week</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="{{ url('/new') }}" class="nav-link">
                <i class="nav-icon fas fa-bullhorn"></i>
                <p>New Berita</p>
            </a>
            </li>
            <li class="nav-item">
            <a href="{{ url('/trending') }}" class="nav-link">
                <i class="nav-icon fas fa-chart-line"></i>
                <p>Trending</p>
            </a>
            </li>
        </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>